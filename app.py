from flask import Flask, render_template
from db.redis_connector import RedisConnector
from db.postgres_connector import PostgresConnector


app = Flask(__name__)


@app.route('/')
def index():
    
    pg_connector = PostgresConnector()
    pg_connector.insert_website_timestamp()
    website_timestamp = pg_connector.get_website_timestamp()
    pg_connector.close_connection()
    
    redis_connector = RedisConnector()
    redis_connector.insert_website_counter()
    website_counter = redis_connector.get_website_counter()
    
    
    return render_template('index.html', website_counter=website_counter, website_timestamp=website_timestamp)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=80)