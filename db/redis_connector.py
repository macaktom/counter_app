
from redis import Redis

class RedisConnector:
    
    def __init__(self, db_params=None):
        self._db_conn = self.get_connection(db_params)
    
    def get_connection(self, db_params=None):
        try:
            if db_params is None:
                db_params = {
                    'host': 'redis',
                    'port': 6379,
                    'decode_responses': True
                }
                
            connection = Redis(**db_params)
            
            return connection
        except Exception as e:
            print("An error occurred while connecting to the database: ", e)
    
                    
    def insert_website_counter(self):
        try:
            self._db_conn.incr('website_counter')
        except Exception as e:
            print(f"An error occurred while inserting the counter: {e}")
        
        
    def get_website_counter(self):
            
        try:
            website_counter = self._db_conn.get('website_counter')
            return website_counter
        except Exception as e:
            print(f"An error occurred while inserting the counter: {e}")
            return None