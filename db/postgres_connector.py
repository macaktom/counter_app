from datetime import datetime
from psycopg2 import OperationalError
import psycopg2
import os

class PostgresConnector:
    
    def __init__(self, db_params=None):
        self._db_conn = self.get_connection(db_params)
    
    def get_connection(self, db_params=None):
        try:
            if db_params is None:
                db_params = {
                    'host': 'postgres',
                    'database': 'counter_app',
                    'user': os.environ['POSTGRES_USER'],
                    'password': os.environ['POSTGRES_PASSWORD']
                }
            connection = psycopg2.connect(**db_params)
            connection.autocommit = True
            
            return connection
        except OperationalError as e:
            print("Connection error: ", e)
        except KeyError as e:
            print("Env variables are not correctly configured: ", e)
        except Exception as e:
            print("An error occurred while connecting to the database: ", e)

    def get_website_timestamp(self):
        try:
            with self._db_conn.cursor() as cursor:
                cursor.execute("SELECT created_at FROM website_visit_timestamp ORDER BY id DESC LIMIT 1")
                website_timestamp = cursor.fetchone()
                
                if not website_timestamp:
                    return None
                else:   
                    return website_timestamp[0].strftime('%Y-%m-%d %H:%M:%S') if website_timestamp else None
                
        except Exception as e:
            print(f"An error occurred while fetching the timestamp: {e}")

    def insert_website_timestamp(self):
        try:
            with self._db_conn.cursor() as cursor:
                timestamp = datetime.now()
                cursor.execute("INSERT INTO website_visit_timestamp (created_at) VALUES (%s)", (timestamp,))
                #print(f"Access timestamp: {timestamp} was successfully stored.")
        except Exception as e:
            print(f"An error occurred while inserting the timestamp: {e}")
            
    
    def close_connection(self):
        if self._db_conn:
            self._db_conn.close()
            self._db_conn = None